package facci.jordanpincha.practica1;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Camara extends AppCompatActivity
implements View.OnClickListener {

    //Request code
    private final int REQUEST_PERMISSION_STORAGE_SAVE = 101;
    private final int REQUEST_PERMISSION_STORAGE_DELETE = 102;
    //Valor de la constante para la camara
    static final int REQUEST_IMAGE_CAPTURE = 1;
    //Instancia de Bitmap para la creacion de la imagen
    Bitmap imageBitmap;
    //Constantes para la gestion de archivos y carpetas
    private final String FOLDER_NAME = "UDCImageApp";
    private final String FILE_NAME = "imageapp.jpg";

    //Views
    private Button buttonOpenImage;
    private ImageView imageView;
    private TextView textViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);
        //Set views
        buttonOpenImage = findViewById(R.id.image_app_btn_capture);
        imageView = findViewById(R.id.image_app_id_picture);
        textViewMessage = findViewById(R.id.image_app_tv_message);

        //Set listeners
        buttonOpenImage.setOnClickListener(this);

        CargarImagenSiExiste();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
            onDeleteMenuTap();
            return true;

        } else if (item.getItemId() == R.id.action_save) {
            onSaveMenuTap();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean hasPermissionsToWrite() {
        return ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void onSaveMenuTap() {
        //CHECK PERMMISSIONS
        if (!hasPermissionsToWrite()) {
            //REQUEST PERMISSIONS
            ActivityCompat.requestPermissions(
                    this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_SAVE);
        } else {
            if (imageView.getDrawable() != null) {
                //CREATE A FOLDER NAMED UDCImageApp
                createFolder();
                //SAVE DIRECTORY
                String storageDir =
                        Environment.getExternalStorageDirectory()+ "/UDCImageApp";
                //SAVE THE PHOTO IN THE SPECIFIED DIR
                createImageFile(storageDir, this.FILE_NAME, imageBitmap);
            }else {
                Toast.makeText(this, "Toma una foto primero!",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onDeleteMenuTap() {
        if (!hasPermissionsToWrite()){
            //request permissions
            ActivityCompat.requestPermissions(
                    this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_STORAGE_DELETE);
        }else {
            //Ventana de diálogo para preguntarle al usuario si está seguro
            //de querer eliminar la imagen
            DialogInterface.OnClickListener dialogClickListener =
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked
                                    try{
                                        deleteImageFile();
                                    }catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No Button clicked
                                    break;
                            }
                        }
                    };

            //Diseñamos la pantalla con la pregunta y las opciones
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Eliminar Imagen")
                    .setMessage("DO you want delete this image?")
                    .setPositiveButton("Si",dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
    }


    String mCurrentPhotoPath;

    private void createImageFile( String storageDir, String fileName, Bitmap bitmap){

        try {
            File myFile = new File(storageDir, fileName);
            FileOutputStream stream = new FileOutputStream(myFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();

            Toast.makeText(this, "Imagen guardada", Toast.LENGTH_LONG).show();

        }
        catch (IOException e){

            e.printStackTrace();
        }

    }

    private void deleteImageFile() throws IOException{
        File storageDir = new File(Environment.getExternalStorageDirectory()+"/UDCImageApp");
        File image = new File(storageDir + "/" + this.FILE_NAME);

        //Si existe el archivo entonces lo elimina, caso contrario
        //envia un mensaje que no se encuentra el archivo
        if(image.exists()){
            image.delete();
            Toast.makeText(this, "Imagen eliminada", Toast.LENGTH_SHORT).show();
            imageView.setImageResource(0);
            textViewMessage.setVisibility(View.VISIBLE);
        }else{
            Toast.makeText(this,"Imagen no encontrada!", Toast.LENGTH_LONG).show();
        }
    }

    private void CargarImagenSiExiste() {
        String myImage = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/" + FOLDER_NAME + "/" + FILE_NAME;
        //OBTENEMOS LA IMAGEN
        File imagen = new File(myImage);
        if (imagen.exists()) {
            //LEER LA IMAGEN DE LA SDCARD UTILIZANDO PICASSO LIBRARY
            Picasso.get().load(imagen).into(imageView);
            textViewMessage.setVisibility(View.INVISIBLE);
        } else {
            textViewMessage.setVisibility(View.VISIBLE);
        }
    }

    private void createFolder() {
        String myfolder = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+ FOLDER_NAME;

        File folder = new File(myfolder);
        if(!folder.exists())
            if (!folder.mkdir()){

                Toast.makeText(this, FOLDER_NAME +  "no se puede crear", Toast.LENGTH_LONG).show();
            }
            else
                Toast.makeText(this, FOLDER_NAME + "creada exitosamente" , Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {
        if (v == buttonOpenImage) {
            TakePictureIntent();
        }

    }

    private void TakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    //DESPUES QUE LA ACTIVITY DE LA CAMARA TOMA LA FOTO Y EL USUARIO ACEPTA LA FOTO
    // ESTA SE ASIGNA AL ImageView PARA SER MOSTRADO, TAMBIEN SE OCULTA EL TEXTVIEW

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //OBTENEMOS DE DATA LA IMAGEN DE LA CAMARA
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            //ASIGNAMOS LA IMAGEN AL ImageView
            imageView.setImageBitmap(imageBitmap);
            //OCULTAMOS EL TEXTVIEW
            textViewMessage.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {

        switch (requestCode){

            case REQUEST_PERMISSION_STORAGE_DELETE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }else{

                    Toast.makeText(this, "Permisom de eliminacion denegado", Toast.LENGTH_SHORT).show();
                }
            }

            case REQUEST_PERMISSION_STORAGE_SAVE:{

                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                    createFolder();
                    String storageDir =
                            Environment.getExternalStorageDirectory()+ "/UDCImageApp/";
                    createImageFile(storageDir, FILE_NAME, imageBitmap);

                }else{

                    Toast.makeText(this, "Permiso de guardar denegado", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}



