package facci.jordanpincha.practica1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Bateria extends AppCompatActivity {

    private TextView mBatteryLevelText;
    private ProgressBar  mBatteryLevelProgress;
    private BroadcastReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bateria);

        Log.e("4C", "OnCreate");

        mBatteryLevelText = findViewById(R.id.textView);
        mBatteryLevelProgress = findViewById(R.id.progressBar);

        mReceiver = new BatteryBroadcastReceiver();
        registerReceiver(mReceiver, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));


    }

    @Override
    protected void onStart() {
        super.onStart();

        registerReceiver(mReceiver,
                new IntentFilter(
                        Intent.ACTION_BATTERY_CHANGED
                ));
        Log.e("4C", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("4C", "onResume");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("4C", "onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("4C", "onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("4C", "onPause");
    }

    private  class BatteryBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(
                    BatteryManager.EXTRA_LEVEL, 0);
            mBatteryLevelText.setText(
                    level + "" + getString(R.string.nivel_de_bateria));
            mBatteryLevelProgress.setProgress(level);
        }
    }
}
