package facci.jordanpincha.practica1;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    Button botonLogin, botonRegistar, botonBuscar, botonBateria;
    Button botonCamara, botonPractica4, botonListado, botonParametro;
    Button btnPractica5;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonParametro = (Button) findViewById(R.id.btnapasarparametro);
        botonLogin = (Button) findViewById(R.id.btnlogin);
        botonRegistar = (Button) findViewById(R.id.btnguardar);
        botonBuscar = (Button) findViewById(R.id.btnbuscar);
        //botonBateria = (Button) findViewById(R.id.btnBateria);
        botonCamara = (Button) findViewById(R.id.btnCamara);
        botonPractica4 = (Button) findViewById(R.id.btnPractica4);
        botonListado = (Button) findViewById(R.id.btnPractica4Listado);
       btnPractica5 = (Button) findViewById(R.id.btnPractica5);
//
        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PasarParametro.class);
                startActivity(intent);
            }
        });

//
        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

        botonRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Guardar.class);
                startActivity(intent);

            }


        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Buscar.class);
                startActivity(intent);

            }
        });

        /* botonBateria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Bateria.class);
                startActivity(intent);
            }
        });


      */  botonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Camara.class);
                startActivity(intent);
            }
        });

        botonPractica4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Practica4.class);
                startActivity(intent);
            }
        });

        botonListado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListadoHeroes.class);
                startActivity(intent);
            }
        });

       btnPractica5.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(MainActivity.this, Practica5.class);
               startActivity(intent);
           }
       });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.activity_login);

                Button btnAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario =(EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave =(EditText) dialogoLogin.findViewById(R.id.txtPassword);

                btnAutenticar.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                        Toast.makeText(MainActivity.this,cajaUsuario.getText().toString()
                                + " " + cajaClave.getText().toString(), Toast.LENGTH_LONG).show();

                    }

                });





                dialogoLogin.show();
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, Guardar.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}