package facci.jordanpincha.practica1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class Practica4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica4);

        RecyclerView recyclerView = findViewById(R.id.recycle_view);

        ArrayList<String> tareas = new ArrayList<>();

        for (int item = 0; item<=10; item++){
            tareas.add("Lista De Tareas");

        }

        TareasRecyclerViewAdapter adapter = new TareasRecyclerViewAdapter(this, tareas);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}


