package facci.jordanpincha.practica1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class ListadoHeroes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_heroes);

        RecyclerView recyclerView = findViewById(R.id.recycle_view);

        ArrayList<String> tareas = new ArrayList<>();

        for (int item = 0; item<=0; item++){
            tareas.add("Listado de Heroes");
            tareas.add("Capitán Ámerica");
            tareas.add("Black Widow");
            tareas.add("Iro Man" );
            tareas.add("Thor");
            tareas.add("Spider-Man");
            tareas.add("Clint Barton");
            tareas.add("Capitana Marvel");
            tareas.add("Doctor Strange");
            tareas.add("Pantera Negra");
            tareas.add("Ant-Man");
            tareas.add("Falcon");
            tareas.add("bruja escarlata");

        }

        TareasRecyclerViewAdapters adapter = new TareasRecyclerViewAdapters(this, tareas);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}