package facci.jordanpincha.practica1;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Practica5 extends AppCompatActivity implements View.OnClickListener {

    EditText cajaCedula, cajaNombres, cajaApellidos;
    Button btnLeer, btnEscribir;
    TextView cajaDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica5);
        cajaCedula = (EditText) findViewById(R.id.txtCedula);
        cajaApellidos = (EditText) findViewById(R.id.txtApellido);
        cajaNombres = (EditText) findViewById(R.id.txtNombre);
        cajaDatos = (TextView) findViewById(R.id.txtDatos);

        btnEscribir = (Button) findViewById(R.id.btnEscribir);
        btnLeer = (Button) findViewById(R.id.btnLeer);

        btnEscribir.setOnClickListener(this);
        btnLeer.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEscribir:
                try {
                    OutputStreamWriter writer = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    writer.write(cajaCedula.getText().toString()+
                            ","+cajaApellidos.getText().toString()+
                            ","+cajaNombres.getText().toString());
                    writer.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error en el archivo de escritura");
                }
                break;
            case R.id.btnLeer:
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = reader.readLine();
                    String [] listaPersonas = datos.split(";");
                    for (int i = 0; i < listaPersonas.length; i++){
                        cajaDatos.append(listaPersonas[i].split(",")[0]+
                                ""+listaPersonas[i].split(",")[1]+
                                ""+listaPersonas[i].split(",")[2]);
                    }

                    //cajaDatos.setText(" ");
                    reader.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error en la lectura del archivo"+ex.getMessage());
                }
                break;
        }

    }
}
